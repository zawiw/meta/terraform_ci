FROM hashicorp/terraform:0.13.4

ARG KUBECTL_VERSION

WORKDIR /root

RUN apk --update --no-cache add jq python3 bash curl ca-certificates git gettext

RUN curl -o kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN install kubectl /usr/local/bin/ && rm kubectl

RUN curl -o rke https://github.com/rancher/rke/releases/download/${RKE_VERSION}/rke_linux-amd64
RUN install rke /usr/local/bin/ && rm rke

ENTRYPOINT ["/bin/bash", "-l", "-c"]
